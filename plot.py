import numpy as np
import matplotlib.pyplot as plt
import math

for i in range(20):
    x = math.sin(18*i)
    y = math.cos(18*i)
    plt.scatter(x,y)

plt.show()