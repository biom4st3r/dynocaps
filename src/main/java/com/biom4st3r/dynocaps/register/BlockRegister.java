package com.biom4st3r.dynocaps.register;

import java.util.function.Supplier;

import com.biom4st3r.dynocaps.ModInit;
import com.biom4st3r.dynocaps.blocks.DynoSifter;
import com.biom4st3r.dynocaps.blocks.DynoSifterBE;
import com.biom4st3r.dynocaps.blocks.DynocapDisplay;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class BlockRegister {
    // BlockEntityType.Builder.create(() -> , EBlock.DISPLAY.asBlock()).build(null)
    public enum EBlock {
        DISPLAY("display", new DynocapDisplay(FabricBlockSettings.copy(Blocks.STONE))),
        SIFTER("sifter", new DynoSifter()),;
        EBlock(String id, Block block) {
            this.block = block;
        }

        private final Block block;

        public Block asBlock() {
            return block;
        }
        public static void loadClass() {
        }
    }

    public enum EBlockEntity {
        DISPLAY("display", () -> new DynocapDisplay.DynocapBlockEntity(), EBlock.DISPLAY.asBlock()),
        SIFTER("sifter", () -> new DynoSifterBE(), EBlock.SIFTER.asBlock()),;
        EBlockEntity(String id, Supplier<BlockEntity> supplier, Block... blocks) {
            this.type = BlockEntityType.Builder.create(supplier, blocks).build(null);
            Registry.register(Registry.BLOCK_ENTITY_TYPE, new Identifier(ModInit.MODID, id), this.type);
        }

        private final BlockEntityType<?> type;

        @SuppressWarnings({"unchecked"})
        public <T extends BlockEntity> BlockEntityType<T> asType() {
            return (BlockEntityType<T>) type;
        }
        public static void loadClass() {
        }
    }
}
