package com.biom4st3r.dynocaps.util;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.stream.Stream;

import net.minecraft.loot.LootGsons;
import net.minecraft.loot.LootPool;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.NbtIo;
import net.minecraft.util.Identifier;
import net.minecraft.util.JsonHelper;

import net.fabricmc.fabric.api.loot.v1.event.LootTableLoadingCallback;
import net.fabricmc.loader.api.FabricLoader;

import com.biom4st3r.dynocaps.BioLogger;
import com.biom4st3r.dynocaps.items.ItemTemplate;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;

public class ResourceHandler {
    public static final BioLogger logger = new BioLogger("ResourceHandler");
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().registerTypeAdapter(ItemTemplate.class, new ItemTemplate.Deserial()).create();

    public static final File ROOT = new File(FabricLoader.getInstance().getGameDir().toFile(), "dynocap_resources");
    public static final File PREFABS_PATH = new File(ROOT, "prefabs");
    public static final File TEMPLATE_ITEMS_PATH = new File(ROOT, "templates");
    public static final File LOOT_TABLE_TWEAKER = new File(ROOT, "tweakers");

    public static Map<Identifier, CompoundTag> PREFABS = Maps.newHashMap();

    private static class Tweaker {
        Tweaker(String id, LootPool[] pools) {
            this.lootTableId = new Identifier(id);
            this.pools = pools;
        }

        public static final Gson GSON = LootGsons.getTableGsonBuilder().setPrettyPrinting().registerTypeAdapter(Tweaker.class, new Tweaker.Serializer()).create();
        public final Identifier lootTableId;
        public final LootPool[] pools;

        public LootTableLoadingCallback get() {
            return (resourceManager, lootManager, identifier, builder, setter) -> {
                if (this.lootTableId.equals(identifier)) {
                    Stream.of(this.pools).forEach(builder::withPool);
                    setter.set(builder.build());
                }
            };
        }

        public static class Serializer implements JsonDeserializer<Tweaker> {
            @Override
            public Tweaker deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                JsonObject obj = json.getAsJsonObject();
                String s = JsonHelper.getString(obj, "target_table");
                // LootPool pool = new LootPool.Serializer().deserialize(JsonHelper.getObject(obj, "pool"), typeOfT, context);
                LootPool[] pools = JsonHelper.deserialize(obj, "pools", context, LootPool[].class);
                return new Tweaker(s, pools);
            }
        }
    }

    public static void loadDefaultDynocap() {
        File defaultDynocap = new File(FabricLoader.getInstance().getConfigDir().toFile(), "Default_Dynocap.json");
        if (defaultDynocap.exists()) {
            try {
                ItemTemplate.LOAD_DEFAULT = ResourceHandler.GSON.fromJson(new InputStreamReader(new FileInputStream(defaultDynocap)), ItemTemplate.class);
                ItemTemplate.DEFAULT = ItemTemplate.LOAD_DEFAULT;
            } catch (JsonSyntaxException | JsonIOException | FileNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            Gson gson = new GsonBuilder().registerTypeAdapter(ItemTemplate.class, new ItemTemplate.Serializer()).setPrettyPrinting().create();
            try {
                defaultDynocap.createNewFile();
                OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(defaultDynocap));
                osw.append(gson.toJson(ItemTemplate.UNINIT_TEMPLATE));
                osw.close();
                ItemTemplate.LOAD_DEFAULT = ItemTemplate.UNINIT_TEMPLATE;
                ItemTemplate.DEFAULT = ItemTemplate.LOAD_DEFAULT;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void init() throws IOException {
        for (File x : new File[]{ROOT, PREFABS_PATH, TEMPLATE_ITEMS_PATH, LOOT_TABLE_TWEAKER}) {
            if (!x.exists()) {
                x.mkdirs();
            }
        }
        logger.log("%sLoading Prefabs:", BioLogger.CYAN);
        Stream
                .of(PREFABS_PATH.listFiles())
                .filter(f -> f.getName().endsWith(".dynobin"))
                .forEach(f -> {
                    try {
                        logger.log("Loading prefab %s", f.getName());
                        DataInputStream dataInputStream = new DataInputStream(new FileInputStream(f));
                        PREFABS.put(new Identifier("dynocaps", f.getName()), NbtIo.read(dataInputStream));
                    } catch (IOException e) {
                        logger.error("FAILED TO LOAD PREFAB %s", f.getName());
                    }
                });
        logger.log("%sLoading Templates:", BioLogger.CYAN);
        Stream
                .of(TEMPLATE_ITEMS_PATH.listFiles())
                .filter(json -> json.getName().endsWith(".json"))
                .forEach(json -> {
                    try {
                        logger.log("Loading item %s", json.getName());
                        ResourceHandler.GSON.fromJson(new InputStreamReader(new FileInputStream(json)), ItemTemplate.class).register();
                    } catch (FileNotFoundException e) {
                        logger.error("FAILED TO LOAD ITEM %s", json.getName());
                    }
                });
        logger.log("%sLoading Tweakers:", BioLogger.CYAN);
        Stream
                .of(LOOT_TABLE_TWEAKER.listFiles())
                .filter(json -> json.getName().endsWith(".json"))
                .<Tweaker>map(json -> {
                    try {
                        logger.log("Loading tweaker %s", json.getName());
                        return Tweaker.GSON.fromJson(new InputStreamReader(new FileInputStream(json)), Tweaker.class);
                    } catch (JsonSyntaxException | JsonIOException | FileNotFoundException e) {
                        throw new RuntimeException(e);
                    }
                })
                .forEach((tweaker) -> {
                    LootTableLoadingCallback.EVENT.register(tweaker.get());
                });
    }
}
