package com.biom4st3r.dynocaps.util;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;

import com.biom4st3r.dynocaps.components.IDynocapComponent;
import com.mojang.datafixers.util.Pair;

import net.fabricmc.fabric.api.renderer.v1.RendererAccess;
import net.fabricmc.fabric.api.renderer.v1.mesh.Mesh;
import net.fabricmc.fabric.api.renderer.v1.mesh.MeshBuilder;
import net.fabricmc.fabric.api.renderer.v1.mesh.QuadEmitter;
import net.fabricmc.fabric.api.renderer.v1.model.FabricBakedModel;
import net.fabricmc.fabric.api.renderer.v1.render.RenderContext;
import net.minecraft.block.BlockState;
import net.minecraft.client.render.model.BakedModel;
import net.minecraft.client.render.model.BakedQuad;
import net.minecraft.client.render.model.ModelBakeSettings;
import net.minecraft.client.render.model.ModelLoader;
import net.minecraft.client.render.model.UnbakedModel;
import net.minecraft.client.render.model.json.JsonUnbakedModel;
import net.minecraft.client.render.model.json.ModelOverrideList;
import net.minecraft.client.render.model.json.ModelTransformation;
import net.minecraft.client.texture.Sprite;
import net.minecraft.client.util.SpriteIdentifier;
import net.minecraft.client.util.math.Vector3f;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;
import net.minecraft.util.Util;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.BlockRenderView;

public class DynocapModel implements UnbakedModel, BakedModel, FabricBakedModel {
    private JsonUnbakedModel unbaked;
    private BakedModel baked;
    private Mesh bakedMesh;
    private int baseColor;

    public DynocapModel(JsonUnbakedModel model, int baseColor) {
        this.unbaked = model;
        this.baseColor = baseColor;
    }

    @Override
    public boolean isVanillaAdapter() {
        return false;
    }

    @Override
    public void emitBlockQuads(BlockRenderView blockView, BlockState state, BlockPos pos, Supplier<Random> randomSupplier, RenderContext context) {
    }

    @Override
    public void emitItemQuads(ItemStack stack, Supplier<Random> randomSupplier, RenderContext context) {
        IDynocapComponent component = IDynocapComponent.TYPE.get(stack);
        context.pushTransform((quad) -> {
            Vector3f VICTIM = new Vector3f();
            quad.copyPos(0, VICTIM);
            if (VICTIM.getY() == 0.5F) { // Bad haxs
                int color = component.getColor();
                quad.spriteColor(0, 0xFF000000 | color, 0xFF000000 | color, 0xFF000000 | color, 0xFF000000 | color);
            } else {
                quad.spriteColor(0, 0xFF000000 | baseColor, 0xFF000000 | baseColor, 0xFF000000 | baseColor, 0xFF000000 | baseColor);
            }
            return true;
        });
        context.meshConsumer().accept(bakedMesh);
        context.popTransform();
    }

    @Override
    public List<BakedQuad> getQuads(BlockState state, Direction face, Random random) {
        return Collections.emptyList();
    }

    @Override
    public boolean useAmbientOcclusion() {
        return baked.useAmbientOcclusion();
    }

    @Override
    public boolean hasDepth() {
        return baked.hasDepth();
    }

    @Override
    public boolean isSideLit() {
        return baked.isSideLit();
    }

    @Override
    public boolean isBuiltin() {
        return false;
    }

    @Override
    public Sprite getSprite() {
        return this.baked.getSprite();
    }

    @Override
    public ModelTransformation getTransformation() {
        return this.baked.getTransformation();
    }

    @Override
    public ModelOverrideList getOverrides() {
        return this.baked.getOverrides();
    }

    @Override
    public Collection<Identifier> getModelDependencies() {
        return this.unbaked.getModelDependencies();
    }

    @Override
    public Collection<SpriteIdentifier> getTextureDependencies(Function<Identifier, UnbakedModel> unbakedModelGetter, Set<Pair<String, String>> unresolvedTextureReferences) {
        return this.unbaked.getTextureDependencies(unbakedModelGetter, unresolvedTextureReferences);
    }

    static final Direction[] QUAD_DIRECTIONS = Util.make(() -> {
        Direction[] dirs = Direction.values();
        dirs = Arrays.copyOf(dirs, dirs.length+1);
        dirs[dirs.length-1] = null;
        return dirs;
    });

    @Override
    public BakedModel bake(ModelLoader loader, Function<SpriteIdentifier, Sprite> textureGetter, ModelBakeSettings rotationContainer, Identifier modelId) {
        this.baked = unbaked.bake(loader, textureGetter, rotationContainer, modelId);
        MeshBuilder builder = RendererAccess.INSTANCE.getRenderer().meshBuilder();
        QuadEmitter emitter = builder.getEmitter();
        Random random = new Random();
        for (Direction dir : QUAD_DIRECTIONS) {
            for (BakedQuad quad : baked.getQuads(null, dir, random)) {
                emitter.fromVanilla(quad, RendererAccess.INSTANCE.getRenderer().materialFinder().find(), null);
                emitter.emit();
            }
        }
        bakedMesh = builder.build();

        return this;
    }
}
