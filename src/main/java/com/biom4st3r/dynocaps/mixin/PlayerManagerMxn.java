package com.biom4st3r.dynocaps.mixin;

import com.biom4st3r.dynocaps.items.ItemTemplate;
import com.biom4st3r.dynocaps.register.Packets;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import net.minecraft.network.ClientConnection;
import net.minecraft.server.PlayerManager;
import net.minecraft.server.network.ServerPlayerEntity;

@Mixin(PlayerManager.class)
public class PlayerManagerMxn {
    @Inject(
            at = @At(
                value = "INVOKE",
                target = "net/minecraft/server/network/ServerPlayNetworkHandler.sendPacket(Lnet/minecraft/network/Packet;)V",
                ordinal = 0,
                shift = Shift.BEFORE),
            method = "onPlayerConnect",
            cancellable = false,
            locals = LocalCapture.NO_CAPTURE)
    public void updateDynocapSettings(ClientConnection connection, ServerPlayerEntity player, CallbackInfo ci) {
        player.networkHandler.sendPacket(Packets.SERVER.sendMaxValues(ItemTemplate.DEFAULT.max_width, ItemTemplate.DEFAULT.max_height, ItemTemplate.DEFAULT.max_depth));
    }
}
