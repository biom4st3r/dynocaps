package com.biom4st3r.dynocaps.mixin.rendering;

import net.minecraft.client.render.block.FluidRenderer;

import com.biom4st3r.dynocaps.rendering.QuadFluidRenderer;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.ModifyConstant;

@Mixin(FluidRenderer.class)
public abstract class FluidRendererMxn {
    /**
     * The Vanilla FluidRenderer only renders per chunk(using a mask of 15).
     *
     * <p>This redirects that 15 to a much better mask for QuadFluidRenderer.
     *
     * @param i
     * @return
     */
    @ModifyConstant(method = "render", constant = @Constant(intValue = 15))
    public int quadFluidRenderer(int i) {
        return ((Object) this) instanceof QuadFluidRenderer ? 0xFFFF_FFFF : i;
    }
}
