package com.biom4st3r.dynocaps.rendering;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.function.Consumer;

import com.biom4st3r.dynocaps.ModInitClient;
import com.biom4st3r.dynocaps.util.ClientHelper;
import com.biom4st3r.dynocaps.util.FadingInt;
import com.google.common.collect.Maps;

import org.apache.logging.log4j.core.util.Assert;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import net.fabricmc.fabric.api.renderer.v1.RendererAccess;
import net.fabricmc.fabric.api.renderer.v1.material.BlendMode;
import net.fabricmc.fabric.api.renderer.v1.mesh.Mesh;
import net.fabricmc.fabric.api.renderer.v1.mesh.MeshBuilder;
import net.fabricmc.fabric.api.renderer.v1.mesh.QuadEmitter;
import net.fabricmc.fabric.api.renderer.v1.render.RenderContext;
import net.fabricmc.fabric.impl.client.indigo.renderer.IndigoRenderer;
import net.fabricmc.fabric.impl.client.indigo.renderer.RenderMaterialImpl;
import net.minecraft.block.BlockState;
import net.minecraft.client.render.BufferBuilder;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.model.BakedModel;
import net.minecraft.client.render.model.BakedQuad;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.client.util.math.Vector3f;
import net.minecraft.client.util.math.Vector4f;
import net.minecraft.util.Util;
import net.minecraft.util.math.Direction;

public class AgnosticRenderContext implements RenderContext {
    static final int FRAPI = RendererAccess.INSTANCE.getRenderer() instanceof IndigoRenderer ? 1 : 0;

    public static final VertexConsumerProvider.Immediate IMMEDIATE = VertexConsumerProvider.immediate(Util.make(() -> {
        Map<RenderLayer, BufferBuilder> builders = Maps.newLinkedHashMapWithExpectedSize(RenderLayer.getBlockLayers().size());
        for (RenderLayer layer : RenderLayer.getBlockLayers()) {
            builders.put(layer, new BufferBuilder(0xFFFF));
        }
        builders.put(RenderLayer.getDirectGlint(), new BufferBuilder(0xFFFF));
        builders.put(RenderLayer.getDirectEntityGlint(), new BufferBuilder(0xFFFF));
        builders.put(RenderLayer.getArmorGlint(), new BufferBuilder(0xFFFF));
        builders.put(RenderLayer.getArmorEntityGlint(), new BufferBuilder(0xFFFF));
        // ItemRenderer
        return builders;
    }), new BufferBuilder(0xFFFF));

    // Map<RenderLayer, VBOState> VBOs;// = Maps.newHashMap();
    Map<RenderLayer, MeshBuilder> meshBuilderMap;
    Map<RenderLayer, Mesh> meshMap;

    public void clear() {
        // for(Clearable buff : this.VBOs.values())
        // {
        //     buff.clear();
        // }
        // this.VBOs.clear();
        this.meshMap.clear();
        this.meshBuilderMap.clear();
        this.currentLayer = null;
        this.lockedLayer = false;
        this.state = null;
    }

    public boolean isEmpty() {
        return meshMap.size() == 0 & meshBuilderMap.size() == 0;
    }

    private void AssertNotNull(String msg, Object o) {
        if (o == null)
            throw new AssertionError(msg);
    }

    public AgnosticRenderContext() {
        meshBuilderMap = Maps.newHashMap();
        meshMap = Maps.newLinkedHashMapWithExpectedSize(RenderLayer.getBlockLayers().size());
    }

    public void render(VertexConsumerProvider vertexConsumerProvider, MatrixStack stack, int light, int overlay) {
        render(vertexConsumerProvider, stack, light, overlay, false);
    }

    public FadingInt fader = new FadingInt(0x99, 0xE5);

    public void render(VertexConsumerProvider vertexConsumerProvider, MatrixStack stack, int light, int overlay, boolean fadingAlpha) {
        //if (loadVBOs(vertexConsumerProvider)) return;
        Vector3f VICTIM_NORMAL = new Vector3f();
        Vector3f VICTIM = new Vector3f();
        Vector4f v4 = new Vector4f();
        float[] colors = new float[4];
        if (meshMap.isEmpty()) {
            // Iterating BlockLayers guarantees corret render order and provides a way to
            // mixin and have your layer rendered
            for (RenderLayer layer : RenderLayer.getBlockLayers()) {
                MeshBuilder builder = meshBuilderMap.get(layer);
                if (builder != null)
                    meshMap.put(layer, builder.build());
            }
        }
        int alpha = fader.next((int) (ClientHelper.client.getTickDelta() * 3));
        for (Entry<RenderLayer, Mesh> entry : meshMap.entrySet()) {
            VertexConsumer[] vc = new VertexConsumer[] { vertexConsumerProvider.getBuffer(entry.getKey()), null };
            entry.getValue().forEach((view) -> {
                if (FRAPI == 1)
                    vc[1] = vertexConsumerProvider.getBuffer(((RenderMaterialImpl) view.material()).blendMode(0) == BlendMode.DEFAULT ? entry.getKey() : ((RenderMaterialImpl) view.material()).blendMode(0).blockRenderLayer);
                RenderHelper.unpackRGBA_F(view.colorIndex(), colors);
                for (int i = 0; i < 4; i++) {
                    int tcolor = view.colorIndex() != -1 ? view.spriteColor(i, 0) : 0xFFFFFFFF;
                    if (fadingAlpha)
                        tcolor = RenderHelper.replaceByte(tcolor, alpha, 24);

                    RenderHelper.unpackRGBA_F(tcolor, colors);
                    view.copyPos(i, VICTIM);
                    if (view.hasNormal(i))
                        view.copyNormal(i, VICTIM_NORMAL);
                    else
                        VICTIM_NORMAL.set(1, 1, 1);

                    v4.set(VICTIM.getX(), VICTIM.getY(), VICTIM.getZ(), 1.0F);
                    v4.transform(stack.peek().getModel());
                    vc[FRAPI].vertex(v4.getX(), v4.getY(), v4.getZ(), colors[0], colors[1], colors[2], colors[3], view.spriteU(i, 0), view.spriteV(i, 0), overlay, view.lightmap(i), VICTIM_NORMAL.getX(), VICTIM_NORMAL.getY(),
                            VICTIM_NORMAL.getZ());
                }
            });
        }
        //populateVBOs(buffers);
    }

    private MeshBuilder getOrInit(RenderLayer layer) {
        Assert.isNonEmpty(layer);
        MeshBuilder builder = meshBuilderMap.get(layer);
        if (builder == null) {
            meshBuilderMap.put(layer, builder = ModInitClient.getRenderer().meshBuilder());
        }
        return builder;
    }

    protected RenderLayer currentLayer;
    protected Random random = new Random();
    protected BlockState state;

    /**
     * Allows this RenderContext to work on all RenderLayers.
     * @param layer
     * @param state
     */
    public void provideContext(RenderLayer layer, BlockState state) {
        if (layer != null && !lockedLayer) this.currentLayer = layer;
        if (state != null) this.state = state;
    }

    boolean lockedLayer = false;

    Consumer<Mesh> meshConsumer = new Consumer<Mesh>() {
        @Override
        public void accept(Mesh mesh) {
            QuadEmitter emitter = getEmitter();
            mesh.forEach((quad) -> {
                quad.copyTo(emitter);
                // FIXME: For some reason copyTo doesn't properly copy lightFace.
                if (!activeTransform.transform(emitter)) return;
                emitter.emit();
            });
        }
    };
    @Override
    public Consumer<Mesh> meshConsumer() {
        return meshConsumer;
    }

    static final Direction[] QUAD_DIRECTIONS = Util.make(() -> {
        int size = Direction.values().length;
        Direction[] QUAD_DIR = Arrays.copyOf(Direction.values(), size+1);
        QUAD_DIR[size] = null;
        return QUAD_DIR;
    });
    Consumer<BakedModel> fallbackConsumer = new Consumer<BakedModel>() {
        @Override
        public void accept(BakedModel model) {
            QuadEmitter emitter = getEmitter();
            for (Direction dir : QUAD_DIRECTIONS) {
                emitQuads(model.getQuads(state, dir, random), emitter, dir);
            }
        }

        private void emitQuads(List<BakedQuad> quads, QuadEmitter emitter, Direction dir) {
            for (BakedQuad quad : quads) {
                emitter.fromVanilla(quad.getVertexData(), 0, false);
                if (quad.hasColor()) emitter.colorIndex(0);
                else emitter.colorIndex(-1);
                emitter.cullFace(dir);
                if (!activeTransform.transform(emitter)) return;
                emitter.emit();
            }
        }
    };
    @Override
    public Consumer<BakedModel> fallbackConsumer() {
        return fallbackConsumer;
    }

    @Override
    public QuadEmitter getEmitter() {
        AssertNotNull("AgnosticRenderContext attempted to load quads without providing RenderLayer", currentLayer);
        return getOrInit(currentLayer).getEmitter();
    }

    ObjectArrayList<QuadTransform> transforms = new ObjectArrayList<>();

    final QuadTransform ITERATE_TRANSFORM = (quadView) -> {
        int i = transforms.size()-1;
        while (i >= 0) {
            if (!transforms.get(i).transform(quadView)) {
                return false;
            }
            i--;
        }
        return true;
    };

    /**
     * From as BlockRenderContext.
     */
    final QuadTransform EMPTY = (quadview) -> true;

    /**
     * From as BlockRenderContext.
     */
    QuadTransform activeTransform = EMPTY;

    @Override
    /**
     * From as BlockRenderContext.
     */
    public void pushTransform(QuadTransform transform) {
        transforms.push(transform);
        if (transforms.size() == 1) {
            activeTransform = transform;
        } else if (transforms.size() > 1) {
            activeTransform = ITERATE_TRANSFORM;
        }
    }

    @Override
    /**
     * From as BlockRenderContext.
     */
    public void popTransform() {
        transforms.pop();
        if (transforms.size() == 0) {
            activeTransform = EMPTY;
        }
    }

    public void lockRenderLayer() {
        AssertNotNull("AgnosticRenderContext attempted to lock render layer without providing a RenderLayer", currentLayer);
        lockedLayer = true;
    }
}
