package com.biom4st3r.dynocaps.rendering;

import net.fabricmc.fabric.api.renderer.v1.mesh.QuadView;
import net.minecraft.client.util.math.Vector3f;
import net.minecraft.util.math.Direction;

public interface QuadDebug {

    @SuppressWarnings("unused")
    static void debug(QuadView view)
    {

        int COLOR_INDEX = view.colorIndex();
        int COLOR0 = view.spriteColor(0, 0);
        int COLOR1 = view.spriteColor(1, 0);
        int COLOR2 = view.spriteColor(2, 0);
        int COLOR3 = view.spriteColor(3, 0);
        
        float U0 = view.spriteU(0, 0);
        float U1 = view.spriteU(1, 0);
        float U2 = view.spriteU(2, 0);
        float U3 = view.spriteU(3, 0);

        float V0 = view.spriteV(0, 0);
        float V1 = view.spriteV(1, 0);
        float V2 = view.spriteV(2, 0);
        float V3 = view.spriteV(3, 0);

        Vector3f POSITION0 = new Vector3f();
        Vector3f POSITION1 = new Vector3f();
        Vector3f POSITION2 = new Vector3f();
        Vector3f POSITION3 = new Vector3f();
        Direction CULL_FACE = view.cullFace();
        boolean HAS_NORMAL0 = view.hasNormal(0);
        boolean HAS_NORMAL1 = view.hasNormal(1);
        boolean HAS_NORMAL2 = view.hasNormal(2);
        boolean HAS_NORMAL3 = view.hasNormal(3);
        Direction LIGHT_FACE = view.lightFace();
        Direction NORMAL_FACE = view.nominalFace();
        view.copyPos(0, POSITION0);
        view.copyPos(1, POSITION1);
        view.copyPos(2, POSITION2);
        view.copyPos(3, POSITION3);
        System.out.println("DEBUG");
    }
    
}