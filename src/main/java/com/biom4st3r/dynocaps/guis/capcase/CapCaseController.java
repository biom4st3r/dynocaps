package com.biom4st3r.dynocaps.guis.capcase;

import io.github.cottonmc.cotton.gui.client.CottonInventoryScreen;
import net.minecraft.entity.player.PlayerEntity;

public class CapCaseController extends CottonInventoryScreen<CapCaseGui> {
    public CapCaseController(CapCaseGui container, PlayerEntity player) {
        super(container, player);
    }

    @Override
    public void onClose() {
        super.onClose();
    }
}
