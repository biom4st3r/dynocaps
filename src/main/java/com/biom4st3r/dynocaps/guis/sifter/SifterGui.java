package com.biom4st3r.dynocaps.guis.sifter;

import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.screen.ScreenHandlerContext;

import com.biom4st3r.dynocaps.register.ScreenHandlerEnum;
import io.github.cottonmc.cotton.gui.SyncedGuiDescription;
import io.github.cottonmc.cotton.gui.widget.WGridPanel;
import io.github.cottonmc.cotton.gui.widget.WItemSlot;

public class SifterGui extends SyncedGuiDescription {

    public SifterGui(int syncId, PlayerInventory playerInventory) {
        this(syncId, playerInventory, ScreenHandlerContext.EMPTY);
    }

    public SifterGui(int syncId, PlayerInventory playerInventory, ScreenHandlerContext ctx) {
        super(ScreenHandlerEnum.SIFTER.getType(), syncId, playerInventory, getBlockInventory(ctx), null);
        WGridPanel root = new WGridPanel(9);
        this.setRootPanel(root);
        root.setSize(30, 20);
        WItemSlot slot = WItemSlot.of(this.blockInventory, 0);
        root.add(slot, 4, 0);
        slot = WItemSlot.of(this.blockInventory, 1);
        root.add(slot, 4, 2);

        root.add(this.createPlayerInventoryPanel(), 0, 4);
        root.validate(this);
    }
}
