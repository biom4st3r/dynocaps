package com.biom4st3r.dynocaps.items;

import java.io.IOException;
import java.io.InputStreamReader;

import net.minecraft.client.render.model.json.JsonUnbakedModel;
import net.minecraft.client.util.ModelIdentifier;
import net.minecraft.resource.Resource;
import net.minecraft.util.Identifier;

import com.biom4st3r.dynocaps.util.DynocapModel;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.model.ModelLoadingRegistry;
import net.fabricmc.loader.api.FabricLoader;

/**
 * Seems like when I have this method in ItemTemplate  CCA manages to load the UnbakedModelClass.
 */
public class ItemTemplateClient {
    @Environment(EnvType.CLIENT)
    public static void client_register(ItemTemplate template, Identifier identifier) {
        if (template.attributes.contains("3dmodel") && !FabricLoader.getInstance().isModLoaded("sodium")) {
            ModelLoadingRegistry.INSTANCE.registerVariantProvider((manager) -> (modelId, context) -> {
                if (modelId.equals(new ModelIdentifier(identifier, "inventory"))) {
                    Resource model = null;
                    try {
                        model = manager.getResource(new Identifier("dynocaps:models/item/dynocap_model.json"));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return new DynocapModel(JsonUnbakedModel.deserialize(new InputStreamReader(model.getInputStream())), template.getModelBaseColor());
                }
                return null;
            });
        }
    }
}
