package com.biom4st3r.dynocaps.blocks;

import java.util.List;

import com.biom4st3r.dynocaps.api.BlockContainer2;
import com.biom4st3r.dynocaps.components.IDynocapComponent;
import com.biom4st3r.dynocaps.items.DynocapItem;
import com.biom4st3r.dynocaps.register.BlockEntityEnum;
import com.google.common.collect.Lists;

import net.fabricmc.fabric.api.screenhandler.v1.ExtendedScreenHandlerFactory;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockEntityProvider;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;

public class DynoSifter extends Block implements BlockEntityProvider {
    // public static Block BLOCK = new DynoSifter();
    // public static BlockEntityType<DynoSifterBE> TYPE = BlockEntityType.Builder.create(() -> new DynoSifterBE(), BLOCK).build(null);

    public DynoSifter() {
        super(AbstractBlock.Settings.copy(Blocks.STONE));
    }

    @Override
    public BlockEntity createBlockEntity(BlockView world) {
        return new DynoSifterBE();
    }


    @Override
    public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand,
            BlockHitResult hit) {
        player.openHandledScreen(state.createScreenHandlerFactory(world, pos));
        return ActionResult.SUCCESS;
    }

    @Override
    public NamedScreenHandlerFactory createScreenHandlerFactory(BlockState state, World world, BlockPos pos) {
        DynoSifterBE be = (DynoSifterBE) world.getBlockEntity(pos);
        return new ExtendedScreenHandlerFactory(){

            @Override
            public Text getDisplayName() {
                return be.getDisplayName();
            }

            @Override
            public ScreenHandler createMenu(int syncid, PlayerInventory inv, PlayerEntity player) {
                return null;
                // return new SifterGui(syncid, inv, ScreenHandlerContext.create(world, pos));
            }

            @Override
            public void writeScreenOpeningData(ServerPlayerEntity player, PacketByteBuf buf) {
            }
        };
    }
}
