package com.biom4st3r.dynocaps.blocks;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.util.Tickable;

import com.biom4st3r.dynocaps.guis.sifter.SifterGui;
import com.biom4st3r.dynocaps.register.BlockEntityEnum;

public class DynoSifterBE extends BlockEntity implements Inventory, Tickable, NamedScreenHandlerFactory {
    Inventory inventory = new SimpleInventory(2) {
        @Override
        public void markDirty() {
            markDirty0();
        }
    };

    public void markDirty0() {
        super.markDirty();
    }
    
    public DynoSifterBE() {
        super(BlockEntityEnum.SIFTER.asType());
    }

    @Override
    public void tick() {
    }

    @Override
    public void clear() {
        this.inventory.clear();
        
    }

    @Override
    public int size() {
        return this.inventory.size();
    }

    @Override
    public boolean isEmpty() {
        return this.inventory.isEmpty();
    }

    @Override
    public ItemStack getStack(int slot) {
        return this.inventory.getStack(slot);
    }

    @Override
    public ItemStack removeStack(int slot, int amount) {
        return this.inventory.removeStack(slot, amount);
    }

    @Override
    public ItemStack removeStack(int slot) {
        return this.inventory.removeStack(slot);
    }

    @Override
    public void setStack(int slot, ItemStack stack) {
        this.inventory.setStack(slot, stack);
    }

    @Override
    public boolean canPlayerUse(PlayerEntity player) {
        return this.inventory.canPlayerUse(player);
    }

    @Override
    public ScreenHandler createMenu(int syncId, PlayerInventory inv, PlayerEntity player) {
        return new SifterGui(syncId, inv, ScreenHandlerContext.create(this.world, this.pos));
    }

    @Override
    public Text getDisplayName() {
        return new LiteralText("string");
    }

    @Override
    public CompoundTag toTag(CompoundTag tag) {
        super.toTag(tag);
        ListTag lt = new ListTag();
        for(int i = 0; i < this.inventory.size(); i++) {
            lt.add(this.inventory.getStack(0).toTag(new CompoundTag()));
        }
        tag.put("item", lt);
        return tag;
    }
    @Override
    public void fromTag(BlockState state, CompoundTag tag) {
        super.fromTag(state, tag);
        ListTag lt = (ListTag) tag.get("items");
        for(int i = 0; i < this.inventory.size(); i++) {
            this.inventory.setStack(i, ItemStack.fromTag(lt.getCompound(i)));
        }
    }

    // @Override
    // public void fromClientTag(CompoundTag tag) {
    //     ListTag lt = (ListTag) tag.get("items");
    //     for(int i = 0; i < this.inventory.size(); i++) {
    //         this.inventory.setStack(i, ItemStack.fromTag(lt.getCompound(i)));
    //     }
    // }

    // @Override
    // public CompoundTag toClientTag(CompoundTag tag) {
    //     ListTag lt = new ListTag();
    //     for(int i = 0; i < this.inventory.size(); i++) {
    //         lt.add(this.inventory.getStack(0).toTag(new CompoundTag()));
    //     }
    //     tag.put("item", lt);
    //     return tag;
    // }
}