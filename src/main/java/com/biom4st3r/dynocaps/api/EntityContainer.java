package com.biom4st3r.dynocaps.api;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.locks.ReentrantLock;

import com.biom4st3r.dynocaps.util.ClientHelper;
import com.google.common.collect.Lists;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.decoration.AbstractDecorationEntity;
import net.minecraft.entity.decoration.painting.PaintingEntity;
import net.minecraft.inventory.Inventory;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class EntityContainer {
    private List<Entity> entities = Lists.newArrayList();
    private ListTag unbakedEntities = null;
    private ReentrantLock LOCK = new ReentrantLock();

    private void borrow(Runnable r) {
        this.LOCK.lock();
        r.run();
        this.LOCK.unlock();
    }

    public Collection<Entity> getEntities() {
        return this.entities;
    }

    private void _add(Entity e, BlockPos ref, World world) {
        BlockPos pos = e.getBlockPos().subtract(ref);
        Entity ee = e.getType().create(world);
        if (ee != null) { // EntityType.PLAYER#create returns null
            ee.copyFrom(e);
            ee.setPos(Math.floor(pos.getX())+0.5, Math.floor(pos.getY()), Math.floor(pos.getZ())+0.5);
            if (e instanceof PaintingEntity && ((PaintingEntity) e).getHeightPixels() / 16 != 1) { // Ugh Haxs
                ee.setPos(ee.getX(), ee.getY()-1, ee.getZ());
            }
            if (e instanceof Inventory) { // ChestMinecartEntity removes its contents when removed
                ((Inventory) e).clear();
            }
            e.remove();
            entities.add(ee);
        }
    }

    public void addToWorld(Entity e) {
        this.borrow(() -> entities.add(e));
    }

    public void add(Entity e, BlockPos ref, World world) {
        this.borrow(() -> this._add(e, ref, world));
    }

    public final void addAll(Iterator<Entity> es, BlockPos ref, World world) {
        this.borrow(() -> {
            while (es.hasNext()) {
                this._add(es.next(), ref, world);
            }
        });
    }

    public void clear() {
        this.borrow(() -> {
            this.entities.clear();
        });
    }

    @Environment(EnvType.CLIENT)
    public final void render(float delta, MatrixStack stack, VertexConsumerProvider vertexConsumers, int light) {
        this.tryBake(ClientHelper.world.get());
        this.borrow(() -> {
            entities.forEach((e) -> {
                Vec3d pos = e.getPos();
                Vec3d tpos = ClientHelper.player.get().getPos();
                e.setPos(tpos.x, 250, tpos.z);
                // ZombieEntityRenderer
                ClientHelper.entityRenderDispatcher.render(e, pos.x, pos.y, pos.z, e.getYaw(ClientHelper.tickDelta.get()), 0, stack, vertexConsumers, 0xF000F0);
                e.setPos(pos.x, pos.y, pos.z);
            });
        });
    }

    public void spawn(World world, BlockPos refPos) {
        this.tryBake(world);
        this.borrow(() -> {
            this.entities.forEach((e) -> {
                BlockPos pos = refPos.add(e.getBlockPos());
                e.resetPosition(pos.getX(), pos.getY(), pos.getZ());
                e.updatePosition(pos.getX(), pos.getY(), pos.getZ());
                world.spawnEntity(e);
            });
        });
        this.clear();
    }

    public void toTag(CompoundTag tag) {
        this.borrow(() -> {
            if (unbakedEntities != null) {
                tag.put("entities", this.unbakedEntities);
                return;
            }
            ListTag lt = new ListTag();
            for (Entity e : this.entities) {
                CompoundTag ct = new CompoundTag();
                e.saveSelfToTag(ct);
                if (e instanceof AbstractDecorationEntity) { // Ugh Haxs
                    ct.putInt("TileX", (int) e.getPos().x);
                    ct.putInt("TileY", (int) e.getPos().y);
                    ct.putInt("TileZ", (int) e.getPos().z);
                }
                lt.add(ct);
            }
            tag.put("entities", lt);
        });
    }

    public void fromTag(CompoundTag tag) {
        this.clear();

        this.unbakedEntities = (ListTag) tag.get("entities");
    }

    private void tryBake(World world) {
        if (this.unbakedEntities != null) {
            this.unbakedEntities.stream().map((ct) -> {
                Optional<EntityType<?>> et = EntityType.fromTag((CompoundTag) ct);
                if (et.isPresent()) {
                    Entity e = et.get().create(world);
                    try {
                        e.fromTag((CompoundTag) ct); // WolfEntity has unchecked cast to ServerWorld
                    } catch (Throwable t) {
                        // do nothing
                    }
                    return e;
                }
                return (Entity) null;
            }).filter((e) -> e != null).forEach((e) -> this.entities.add(e));
            this.unbakedEntities = null;
        }
    }
}
