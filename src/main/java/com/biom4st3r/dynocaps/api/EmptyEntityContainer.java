package com.biom4st3r.dynocaps.api;

import net.minecraft.entity.Entity;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class EmptyEntityContainer extends EntityContainer {

    public static final EntityContainer EMPTY = new EmptyEntityContainer();

    @Override
    public void add(Entity e, BlockPos ref, World world) {
    }

    @Override
    public void clear() {
    }

    @Override
    public void fromTag(CompoundTag tag) {
    }

    @Override
    public void spawn(World world, BlockPos refPos) {
    }

    @Override
    public void toTag(CompoundTag tag) {
    }
    
}
