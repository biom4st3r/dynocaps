package com.biom4st3r.dynocaps.features;

import com.biom4st3r.dynocaps.ModInitClient;
import com.biom4st3r.dynocaps.components.IDynocapComponent;
import com.biom4st3r.dynocaps.register.ItemEnum;
import com.biom4st3r.dynocaps.rendering.AgnosticRenderContext;
import com.biom4st3r.dynocaps.rendering.RenderHelper;
import com.biom4st3r.dynocaps.util.ClientHelper;

import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.render.OverlayTexture;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.texture.Sprite;
import net.minecraft.client.texture.TextureManager;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Vec3d;

public class BoxRenderer {
    // Don't change. Canvas hooks this.
    @SuppressWarnings("deprecation")
    public static void render(MatrixStack stack, VertexConsumerProvider.Immediate oldImmediate, int light, Vec3d camPos) {
        if (!ModInitClient.doRendering) return;
        VertexConsumer vc = AgnosticRenderContext.IMMEDIATE.getBuffer(RenderLayer.getLines());
        light = 0x00F000F0;

        ClientPlayerEntity entity = ClientHelper.player.get();
        if (ClientHelper.client.currentScreen != null) return;
        ItemStack hand = entity.getMainHandStack();
        if (ItemEnum.DYNOCAPS.contains(hand.getItem())) {
            IDynocapComponent component = IDynocapComponent.TYPE.get(hand);

            stack.push();
            stack.translate(entity.getX()-camPos.x, entity.getY()-camPos.y, entity.getZ()-camPos.z);
            float boxWidth = component.getWidth();
            float boxHeight = component.getHeight();
            float boxDepth = component.getDepth();
            Direction dir = entity.getHorizontalFacing();
            Sprite sprite = RenderHelper.getBlockSprite(TextureManager.MISSING_IDENTIFIER);
            double xoff = entity.getX();
            double yoff = entity.getY();
            double zoff = entity.getZ();

            xoff = xoff - Math.floor(xoff);
            yoff = yoff - Math.floor(yoff);
            zoff = zoff - Math.floor(zoff);
            stack.translate(-xoff, -yoff+0.5f, -zoff);
            int color = component.getColor();

            stack.push();
            // stack.translate(0, , 0);
            if (dir == Direction.SOUTH) {
                stack.translate(-Math.floor(boxWidth/2f), -0.5f, boxDepth+1);
            } else if (dir == Direction.EAST) {
                stack.translate(2, -0.5f, Math.floor(boxDepth/2f) - (boxDepth % 2 == 0 ? 1 : 0));
            } else if (dir == Direction.NORTH) {
                stack.translate(-Math.floor(boxWidth/2f), -0.5f, -2);
            } else if (dir == Direction.WEST) {
                stack.translate(-boxWidth-1, -0.5f, Math.floor(boxDepth/2f) - (boxDepth % 2 == 0 ? 1 : 0));
            }
            RenderHelper.renderBox(0, 0, 0, boxWidth, boxDepth, boxHeight, 1, sprite, vc, stack, light, color);

            stack.pop();
            if (component.isFilled()) {
                stack.push();
                stack.translate(0, -0.5f, 0);
                if (dir == Direction.SOUTH) {
                    stack.translate(-(int) (boxWidth / 2F), 0, 2);
                } else if (dir == Direction.EAST) {
                    stack.translate(2, 0, -(int) (boxDepth / 2F));
                } else if (dir == Direction.NORTH) {
                    stack.translate(-(int) (boxWidth / 2F), 0, -boxDepth-1);
                } else if (dir == Direction.WEST) {
                    stack.translate(-boxWidth-1, 0, -(int) (boxDepth / 2F));
                }
                if (component.getCache().isEmpty()) {
                    component.getCache().clear();
                    component.getRenderer().clear();
                    component.getCache().provideContext(RenderLayer.getTranslucent(), null); // only for providing a locked renderlayer
                    component.getCache().lockRenderLayer();
                    component.getRenderer().fillQuadCache(component);
                }
                component.getRenderer().renderBlockEntities(ClientHelper.client.getTickDelta(), stack, AgnosticRenderContext.IMMEDIATE, 0x800080);
                component.getCache().render(AgnosticRenderContext.IMMEDIATE, stack, 0x800080, OverlayTexture.DEFAULT_UV, true);
                component.getEntities().render(ClientHelper.client.getTickDelta(), stack, AgnosticRenderContext.IMMEDIATE, light);

                stack.pop();
            }
            stack.pop();
            AgnosticRenderContext.IMMEDIATE.draw();
        }
    }
}
