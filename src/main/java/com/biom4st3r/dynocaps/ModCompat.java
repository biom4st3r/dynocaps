package com.biom4st3r.dynocaps;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.biom4st3r.dynocaps.components.IDynocapComponent;
import com.biom4st3r.dynocaps.components.IDynocapComponent.Result;
import com.biom4st3r.dynocaps.util.reflection.FieldRef;
import com.biom4st3r.dynocaps.util.reflection.MethodRef;
import com.google.common.collect.Lists;
import com.jamieswhiteshirt.rtree3i.Entry;
import com.jamieswhiteshirt.rtree3i.Selection;

import org.jetbrains.annotations.Nullable;

import draylar.goml.api.ClaimBox;
import draylar.goml.api.ClaimInfo;
import draylar.goml.api.ClaimUtils;
import io.github.flemmli97.flan.api.PermissionRegistry;
import io.github.flemmli97.flan.claim.Claim;
import io.github.flemmli97.flan.claim.ClaimStorage;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.LiteralText;
import net.minecraft.util.Formatting;
import net.minecraft.util.Util;
import net.minecraft.util.math.BlockPos;

public final class ModCompat {
    public static boolean hasGOML = FabricLoader.getInstance().isModLoaded("goml");
    public static boolean hasFlan = FabricLoader.getInstance().isModLoaded("flan");

    public interface Compatible {
        Result test(BlockPos bottomleft, ServerWorld world, @Nullable PlayerEntity player, IDynocapComponent component);
    }

    public static boolean shouldTestForCompat() {
        return hasGOML || hasFlan;
    }

    static Compatible EMPTY = (bottomleft, world, user, component) -> {
        return Result.SUCCESS;
    };

    static Compatible get(Compatible c, boolean test) {
        return test ? c : EMPTY;
    }

    public static Result testForProtection(BlockPos bottomleft, ServerWorld world, @Nullable PlayerEntity player, IDynocapComponent component) {
        Result result = Result.SUCCESS;
        Iterator<Compatible> compats = Lists.newArrayList(get(GomlTest, hasGOML), get(FlanTest, hasFlan)).iterator();
        while (result == Result.SUCCESS && compats.hasNext()) {
            result = compats.next().test(bottomleft, world, player, component);
        }
        return result;
    }

    public static Compatible GomlTest = (bottomleft, world, user, component) -> {
        BlockPos top = new BlockPos(bottomleft.getX() + component.getHeight(), bottomleft.getY() + component.getHeight(), bottomleft.getZ() + component.getWidth());
        Selection<Entry<ClaimBox, ClaimInfo>> claims = ClaimUtils.getClaimsInBox(world, bottomleft, top);
        boolean[] hasPermission = {true};
        if (user == null) return claims.count() == 0 ? Result.SUCCESS : Result.GOLM;
        claims.forEach((entry) -> {
            hasPermission[0] &= ClaimUtils.playerHasPermission(entry, user);
        });
        return hasPermission[0] ? Result.SUCCESS : Result.GOLM;
    };

    static MethodRef<Set<Claim>> Flan$ClaimStorage$conflicts;
    static FieldRef<List<Claim>> Flan$Claim$subClaim;
    static FieldRef<Integer> Flan$Claim$minX;
    static FieldRef<Integer> Flan$Claim$minY;
    static FieldRef<Integer> Flan$Claim$minZ;

    public static List<Claim> getUnrolledConflictingClaims(ClaimStorage storage, Claim claim) {
        List<Claim> claimsInArea = Lists.newArrayList(Flan$ClaimStorage$conflicts.invoke(storage, claim, null));
        if (claimsInArea.isEmpty()) return Collections.emptyList();
        for (int i = 0; i < claimsInArea.size();) {
            Claim t = claimsInArea.get(i);
            List<Claim> subClaims = (Flan$Claim$subClaim.getOrSet(t, null));
            if (!subClaims.isEmpty()) {
                claimsInArea.addAll(subClaims);
                claimsInArea.remove(t);
            } else i++;
        }
        return claimsInArea;
    }

    public static boolean flan_isDispenserPermitted(BlockPos dispenserPos, BlockPos dynocapOffsetPos, ServerWorld world, IDynocapComponent component) {
        List<Claim> dispensersConflicts = getUnrolledConflictingClaims(ClaimStorage.get(world), new Claim(dispenserPos, dispenserPos, Util.NIL_UUID, world));
        List<Claim> dynocapConflicts = getUnrolledConflictingClaims(ClaimStorage.get(world), dynocapToClaim(dynocapOffsetPos, component, world));

        if (dispensersConflicts.size() == 1 && dynocapConflicts.size() == 1 && dispensersConflicts.get(0) == dynocapConflicts.get(0)) {
            return true;
        }

        return dynocapConflicts.isEmpty();
    }

    public static boolean goml_isDispenserPermitted(BlockPos dispenserPos, BlockPos dynocapOffsetPos, ServerWorld world, IDynocapComponent component) {
        return false;
    }

    public static Claim dynocapToClaim(BlockPos bottomleft, IDynocapComponent component, ServerWorld world) {
        return new Claim(bottomleft, bottomleft.add(component.getWidth(), component.getHeight(), component.getDepth()), Util.NIL_UUID, world);
    }

    public static Compatible FlanTest = (bottomleft, world, user, component) -> {
        try {
            if (Flan$ClaimStorage$conflicts == null) {
                Flan$ClaimStorage$conflicts = MethodRef.getMethod(ClaimStorage.class, "conflicts", Claim.class, Claim.class);
                Flan$Claim$subClaim = FieldRef.getFieldGetter(Claim.class, "subClaims", -1);
                Flan$Claim$minX = FieldRef.getFieldGetter(Claim.class, "minX", -1);
                Flan$Claim$minY = FieldRef.getFieldGetter(Claim.class, "minY", -1);
                Flan$Claim$minZ = FieldRef.getFieldGetter(Claim.class, "minZ", -1);
            }
            ClaimStorage storage = ClaimStorage.get(world);
            Claim dynoClaim = dynocapToClaim(bottomleft, component, world);

            List<Claim> claimsInArea = getUnrolledConflictingClaims(storage, dynoClaim);

            if (claimsInArea.isEmpty()) return Result.SUCCESS;
            else if (user == null) return Result.FLAN;

            boolean perm = true;
            BlockPos.Mutable mutPos = new BlockPos.Mutable();

            for (Claim claim : claimsInArea) {
                mutPos.set(Flan$Claim$minX.getOrSet(claim, null), Flan$Claim$minY.getOrSet(claim, null), Flan$Claim$minZ.getOrSet(claim, null));
                perm &= claim.canInteract((ServerPlayerEntity) user, PermissionRegistry.BREAK, mutPos.toImmutable());
            }

            return perm ? Result.SUCCESS : Result.FLAN;
        } catch (Throwable e) {
            e.printStackTrace();
            user.sendSystemMessage(new LiteralText("Flan compatibility is broken for dynocaps. Please Notify Biom4st3r!").formatted(Formatting.RED), Util.NIL_UUID);
            ModInit.logger.error("\n\n\n\nFlan compatiblity is broken for Dynocaps. Please notifty Biom4st3r!\n\n\n\n");
            return Result.FLAN;
        }
    };
}
