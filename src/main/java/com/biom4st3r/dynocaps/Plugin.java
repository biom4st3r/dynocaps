package com.biom4st3r.dynocaps;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import net.fabricmc.loader.api.FabricLoader;

import com.google.common.io.Resources;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.spongepowered.asm.mixin.extensibility.IMixinConfigPlugin;
import org.spongepowered.asm.mixin.extensibility.IMixinInfo;

public class Plugin implements IMixinConfigPlugin {
    public static BioLogger logger = new BioLogger("dyno:plugin");

    @Override
    public void onLoad(String mixinPackage) {
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_MAXS | ClassWriter.COMPUTE_FRAMES);
        ClassVisitor cv = new ClassVisitor(Opcodes.ASM8, cw) {
        };
        MethodVisitor mv = cv.visitMethod(Opcodes.ACC_PUBLIC | Opcodes.ACC_STATIC | Opcodes.ACC_FINAL, "<wtf>", "()V", null, null);
        mv.visitCode();
        mv.visitLdcInsn("<wtf>");
        mv.visitMethodInsn(Opcodes.INVOKESPECIAL, "java.lang.Throwable", "<init>", "(Ljava/lang/String;)V", false);
        mv.visitInsn(Opcodes.ATHROW);
        mv.visitInsn(Opcodes.RETURN);
        mv.visitEnd();

        try {
            ClassReader reader = new ClassReader(Resources.toByteArray(Thread.currentThread().getContextClassLoader().getResource("com/biom4st3r/dynocaps/ModInit.class")));
            reader.accept(cv, 0);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public String getRefMapperConfig() {
        return null;
    }

    @Override
    public boolean shouldApplyMixin(String targetClassName, String mixinClassName) {
        if (mixinClassName.contains("debug.")) return FabricLoader.getInstance().isDevelopmentEnvironment();
        return true;
    }

    @Override
    public void acceptTargets(Set<String> myTargets, Set<String> otherTargets) {
    }

    @Override
    public List<String> getMixins() {
        return null;
    }

    @Override
    public void preApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo) {
    }

    @Override
    public void postApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo) {
    }

    public void write(ClassNode targetClass) {
        File file = new File(FabricLoader.getInstance().getConfigDir().toString(), "BYTECODE.class");
        ClassWriter writer = new ClassWriter(ClassReader.EXPAND_FRAMES);
        targetClass.accept(writer);

        FileOutputStream fos;

        try {
            fos = new FileOutputStream(file);
            fos.write(writer.toByteArray());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        throw new IllegalStateException();
    }
}
