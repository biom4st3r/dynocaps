package com.biom4st3r.dynocaps.dynoworld;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Consumer;
import java.util.Set;

import com.biom4st3r.dynocaps.api.ProtoWorld;

import it.unimi.dsi.fastutil.longs.LongOpenHashBigSet;
import it.unimi.dsi.fastutil.longs.LongSet;
import it.unimi.dsi.fastutil.shorts.ShortList;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.Entity;
import net.minecraft.fluid.Fluid;
import net.minecraft.fluid.FluidState;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.structure.StructureStart;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.BlockRenderView;
import net.minecraft.world.Heightmap;
import net.minecraft.world.Heightmap.Type;
import net.minecraft.world.TickScheduler;
import net.minecraft.world.World;
import net.minecraft.world.biome.source.BiomeArray;
import net.minecraft.world.chunk.ChunkSection;
import net.minecraft.world.chunk.ChunkStatus;
import net.minecraft.world.chunk.UpgradeData;
import net.minecraft.world.chunk.WorldChunk;
import net.minecraft.world.gen.feature.StructureFeature;

public class DynoChunk extends WorldChunk {
    public DynoChunk(World world, ChunkPos pos, BiomeArray biomes, UpgradeData upgradeData,
            TickScheduler<Block> blockTickScheduler, TickScheduler<Fluid> fluidTickScheduler, long inhabitedTime,
            ChunkSection[] sections, Consumer<WorldChunk> loadToWorldConsumer) {
        super(world, pos, biomes, upgradeData, blockTickScheduler, fluidTickScheduler, inhabitedTime, sections,
                loadToWorldConsumer);
    }

    BlockRenderView view;

    ChunkPos chunkPos;

    ProtoWorld pWorld;

    BlockPos chunkRel(BlockPos pos) {
        return new BlockPos(new BlockPos((16 * this.chunkPos.x) + (pos.getX() & 15), pos.getY(), (16 * this.chunkPos.z) + (pos.getZ() & 15)));
    }

    @Override
    public BlockEntity getBlockEntity(BlockPos pos) {
        return pWorld.getBlockEntity(chunkRel(pos));
    }

    @Override
    public BlockState getBlockState(BlockPos pos) {
        return pWorld.getBlockState(chunkRel(pos));
    }

    @Override
    public FluidState getFluidState(BlockPos pos) {
        return pWorld.getFluidState(chunkRel(pos));
    }

    @Override
    public StructureStart<?> getStructureStart(StructureFeature<?> structure) {
        // get
        return null;
    }

    @Override
    public void setStructureStart(StructureFeature<?> structure, StructureStart<?> start) {
        // put
    }

    @Override
    public LongSet getStructureReferences(StructureFeature<?> structure) {
        return new LongOpenHashBigSet();
    }

    @Override
    public void addStructureReference(StructureFeature<?> structure, long reference) {
        // computeIfAbsent
    }

    @Override
    public Map<StructureFeature<?>, LongSet> getStructureReferences() {
        return Collections.emptyMap();
    }

    @Override
    public void setStructureReferences(Map<StructureFeature<?>, LongSet> structureReferences) {
        // putAll
    }

    @Override
    public BlockState setBlockState(BlockPos pos, BlockState state, boolean moved) {
        if (pWorld.setBlockState(chunkRel(pos), state, 0)) return state;
        return Blocks.AIR.getDefaultState();
    }

    @Override
    public void setBlockEntity(BlockPos pos, BlockEntity blockEntity) {
        pWorld.setBlockEntity(pos, blockEntity);
    }

    @Override
    public void addEntity(Entity entity) {
        pWorld.addEntity(entity);
    }

    @Override
    public Set<BlockPos> getBlockEntityPositions() {
        return null;
    }

    @Override
    public ChunkSection[] getSectionArray() {
        return null;
    }

    @Override
    public Collection<Entry<Type, Heightmap>> getHeightmaps() {
        return null;
    }

    @Override
    public void setHeightmap(Type type, long[] heightmap) {
    }

    @Override
    public Heightmap getHeightmap(Type type) {
        return null;
    }

    @Override
    public int sampleHeightmap(Type type, int x, int z) {
        return 0;
    }

    @Override
    public ChunkPos getPos() {
        return this.chunkPos;
    }

    @Override
    public void setLastSaveTime(long lastSaveTime) {
    }

    @Override
    public Map<StructureFeature<?>, StructureStart<?>> getStructureStarts() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setStructureStarts(Map<StructureFeature<?>, StructureStart<?>> structureStarts) {
    }

    @Override
    public BiomeArray getBiomeArray() {
        return null;
    }

    @Override
    public void setShouldSave(boolean shouldSave) {
    }

    @Override
    public boolean needsSaving() {
        return false;
    }

    @Override
    public ChunkStatus getStatus() {
        return null;
    }

    @Override
    public void removeBlockEntity(BlockPos pos) {
    }

    @Override
    public ShortList[] getPostProcessingLists() {
        return null;
    }

    @Override
    public CompoundTag getBlockEntityTag(BlockPos pos) {
        return null;
    }

    @Override
    public CompoundTag getPackedBlockEntityTag(BlockPos pos) {
        return null;
    }

    @Override
    public boolean isLightOn() {
        return false;
    }

    @Override
    public void setLightOn(boolean lightOn) {
    }
}
