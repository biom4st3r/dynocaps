package com.biom4st3r.dynocaps.components;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.function.Predicate;
import java.util.stream.Stream;

import com.biom4st3r.dynocaps.ModCompat;
import com.biom4st3r.dynocaps.ModInit;
import com.biom4st3r.dynocaps.api.BlockContainer;
import com.biom4st3r.dynocaps.api.BlockContainer2;
import com.biom4st3r.dynocaps.api.BlockInstance;
import com.biom4st3r.dynocaps.api.EntityContainer;
import com.biom4st3r.dynocaps.api.OnBlockCapture;
import com.biom4st3r.dynocaps.items.ItemTemplate;
import com.biom4st3r.dynocaps.rendering.AgnosticRenderContext;
import com.biom4st3r.dynocaps.rendering.DynocapRenderer;
import com.biom4st3r.dynocaps.util.reflection.FieldRef;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.io.FastByteArrayInputStream;
import it.unimi.dsi.fastutil.io.FastByteArrayOutputStream;
import it.unimi.dsi.fastutil.longs.Long2BooleanMap;
import it.unimi.dsi.fastutil.longs.Long2BooleanMap.Entry;
import it.unimi.dsi.fastutil.longs.Long2BooleanOpenHashMap;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.block.AbstractSignBlock;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.WallSignBlock;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.SignBlockEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.NbtIo;
import net.minecraft.particle.ParticleEffect;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.state.property.Properties;
import net.minecraft.tag.FluidTags;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockPos.Mutable;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Vec3i;

public class DynocapComponent implements IDynocapComponent {
    static Class<?> gunpowderSignType;
    static FieldRef<Object> SignBlockEntity$getSignType;
    static {
        try {
            gunpowderSignType = Class.forName("io.github.gunpowder.entities.builders.SignType");
            Field f = Stream.of(SignBlockEntity.class.getDeclaredFields()).filter(field->field.getType().equals(gunpowderSignType)).findFirst().get();
            f.setAccessible(true);
            SignBlockEntity$getSignType = ()->f;
        } catch (ClassNotFoundException e) {
            SignBlockEntity$getSignType = new FieldRef<Object>(){
                @Override
                public Object getOrSet(Object host, Object value) {
                    return null;
                };
                @Override
                public Field getField() throws Throwable {
                    return null;
                }                
            };
            gunpowderSignType = null;
        }
    }

    // @Override
    @SuppressWarnings({"deprecation"})
    public boolean isComponentEqual(nerdhub.cardinal.components.api.component.Component other) {
        return other instanceof DynocapComponent && ((DynocapComponent) other).equals(this);
    }

    @Override
    public void copyFrom(IDynocapComponent other) {
        this.width = other.getWidth();
        this.height = other.getHeight();
        this.depth = other.getDepth();
        // this.container = other.getContainer().getCopy();
        this.container = other.getContainer().getCopy();
        this.color = other.getColor();
        this.name = other.getName();
        this.hashcode = other.hashCode();
        this.shouldPlaceAir = other.shouldPlaceAir();
    }

    protected int width = 1, depth = 1, height = 1;

    protected BlockContainer2 container;
    protected EntityContainer entity_container;
    // protected BlockContainer container;
    protected int hashcode = -1;
    protected int color;
    protected boolean shouldPlaceAir;
    protected String name;

    public final ItemTemplate template;

    public DynocapComponent(ItemTemplate template) {
        this.template = template;
        // this.container = new BlockContainer(0);
        this.container = new BlockContainer2(0);
        this.entity_container = new EntityContainer();
        this.width = template.default_width;
        this.depth = template.default_depth;
        this.height = template.default_height;
        this.color = template.color;
        this.name = template.default_name;
        this.shouldPlaceAir = true;
        this.hashcode = System.identityHashCode(this);
    }

    @Override
    public ItemTemplate getTemplate() {
        return this.template;
    }

    @Override
    public int hashCode() {
        return hashcode;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public int getDepth() {
        return depth;
    }

    private Long2BooleanMap generateSkipMap(ServerWorld world, BlockPos bottomLeft) {
        BlockPos.Mutable pos = bottomLeft.mutableCopy();
        Long2BooleanOpenHashMap toCap = new Long2BooleanOpenHashMap(this.width * this.height * this.depth);
        for (int y = getHeight() - 1; y >= 0; y--) {
            for (int x = 0; x < width; x++) {
                for (int z = 0; z < depth; z++) {
                    pos.set(bottomLeft.getX() + x, bottomLeft.getY() + y, bottomLeft.getZ() + z);
                    BlockState currState = world.getBlockState(pos);
                    if (currState.getBlock() instanceof AbstractSignBlock && 
                            SignBlockEntity$getSignType.getOrSet(world.getBlockEntity(pos.toImmutable()), null) != null) {
                        if (currState.getBlock() instanceof WallSignBlock) {
                            toCap.put(pos.toImmutable().offset(currState.get(WallSignBlock.FACING).getOpposite()).asLong(), false);
                        } else {
                            toCap.put(pos.toImmutable().offset(Direction.DOWN).asLong(), false);
                        }
                        toCap.put(pos.asLong(), false);
                    } else if (this.template.preventCaptureBlocks.contains(currState.getBlock())) {
                        toCap.put(pos.asLong(), false);
                    } else {
                        toCap.computeIfAbsent(pos.asLong(), l->true);
                    }
                }
            }
        }
        return toCap;
    }

    @Override
    public Result unprotectedCaptureBlocks(ServerWorld world, BlockPos bottomLeft, PlayerEntity player, boolean destroy) {
        Result r = checkheight(bottomLeft);
        if (!r.isSuccess()) {
            return r;
        }
        container.reinit(this.width * this.height * this.depth);

        BlockPos.Mutable pos = new Mutable();
        Long2BooleanMap toCap = generateSkipMap(world, bottomLeft);

        for(Entry entry : toCap.long2BooleanEntrySet()) {
            pos.set(entry.getLongKey());
            if(!entry.getBooleanValue()) {
                container.addEmpty(bottomLeft, pos);
                continue;
            }
            container.add(world, bottomLeft, pos);
            BlockEntity be = world.getBlockEntity(pos); 
            BlockInstance instance = container.getLatest();
            if (destroy) {
                if (instance.isBlockEntity()) {
                    if (!OnBlockCapture.actions.get(be.getType()).postCapture(world, pos, be, instance.state, player)) {
                        container.revokeLatest();
                        container.addEmpty(bottomLeft, pos);
                        continue;
                    }
                }
                BlockState block = world.getBlockState(pos);
                world.setBlockState(pos, Blocks.AIR.getDefaultState(), 0b110111); // Is it okay that this goes first?
                try {
                    if (player != null) block.getBlock().onBreak(world, pos, block, player);
                } catch (Throwable t) {
                    // do nothing
                }
            }
        }
        if (this.getTemplate().canCaptureEntities()) entity_container.addAll(
            world.getEntitiesByClass(Entity.class, new Box(bottomLeft, bottomLeft.add(this.getWidth(), this.getHeight(), this.getDepth())), (f) -> !this.template.preventCaptureEntities.contains(f.getType())).iterator(),
            bottomLeft, world);
        container.trim();
        this.hashcode = container.hashCode();
        return Result.SUCCESS;
    }

    /**
     * flag 0b110011 is used to stop blocks from changing as their neighbors are
     * removed such as stairs or fences unlinking.
     */
    @Override
    public Result captureBlocks(ServerWorld world, BlockPos bottomLeft, PlayerEntity player, boolean destroy) {
        if (ModCompat.shouldTestForCompat()) {
            if (!(player != null && player.hasPermissionLevel(2) && player.isCreative())) {
                Result result = ModCompat.testForProtection(bottomLeft, (ServerWorld) world, player, this);
                if (!(result == Result.SUCCESS)) {
                    return result;
                }
            }
        }
        return unprotectedCaptureBlocks(world, bottomLeft, player, destroy);
    }

    @Override
    public BlockContainer2 getContainer() {
        return this.container;
    }

    @Override
    public EntityContainer getEntities() {
        return this.entity_container;
    }

    public static final boolean testForEachPos(Box box, Predicate<BlockPos> function) {
        BlockPos.Mutable mut = new BlockPos.Mutable();
        for (int y = (int) box.minY; y <= box.maxY; y++) {
            for (int x = (int) box.minX; x <= box.maxX; x++) {
                for (int z = (int) box.minZ; z <= box.maxZ; z++) {
                    mut.set(x, y, z);
                    if (!function.test(mut)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private Result checkheight(BlockPos pos) {
        if (pos.getY() + this.getHeight() > 256) {
            return Result.MAX_HEIGHT;
        } else if (pos.getY() < 0) {
            return Result.MIN_HEIGHT;
        }
        return Result.SUCCESS;
    }

    @Override
    public Result unprotectedReleaseBlocks(ServerWorld world, BlockPos startPos, PlayerEntity user) {
        Result[] result = {checkheight(startPos)};
        if (!result[0].isSuccess()) {
            return result[0];
        }
        Box box = new Box(startPos, startPos.add(new Vec3i(this.width - 1, this.height - 1, this.depth - 1)));
        if (this.template.requiresEmptyArea() && !testForEachPos(box, (bp) -> {
            BlockState state = world.getBlockState(bp);
            boolean isWater = state.getFluidState().isIn(FluidTags.WATER);
            boolean replacable = state.getMaterial().isReplaceable();
            if (!(isWater || replacable)) {
                return false;
            }
            return true;
        })) {
            
            return Result.PREVENT_OVERRIDES;
        }
        container.forEach((instance) -> { // forEachReversed
            if (!this.shouldPlaceAir && instance.state.isAir()) {
                return;
            }
            BlockPos offsetPos = BlockPos.fromLong(startPos.asLong()).add(instance.relativePos);
            if (this.template.preventOverrideBlocks.contains(world.getBlockState(offsetPos).getBlock())) {
                result[0] = Result.PROHIBIITED_BLOCK;
                return;
            }
            if (world.getDimension().isUltrawarm() && !instance.state.getFluidState().isEmpty()) {
                if (instance.state.getBlock() == Blocks.WATER) {
                    instance.state = Blocks.AIR.getDefaultState();
                } else if (instance.state.contains(Properties.WATERLOGGED) && instance.state.get(Properties.WATERLOGGED)) {
                    instance.state = instance.state.with(Properties.WATERLOGGED, false);
                }
                world.setBlockState(offsetPos, Blocks.AIR.getDefaultState());
                world.playSound(null, startPos, SoundEvents.BLOCK_FIRE_EXTINGUISH, SoundCategory.BLOCKS, 0.5F, 2.6F + (world.random.nextFloat() - world.random.nextFloat()) * 0.8F);

                for (int l = 0; l < 8; ++l) {
                    world.addParticle(ParticleTypes.LARGE_SMOKE, (double) offsetPos.getX() + Math.random(), (double) offsetPos.getY() + Math.random(), (double) offsetPos.getZ() + Math.random(), 0.0D, 0.0D, 0.0D);
                }
            } else {
                BlockState currentState = world.getBlockState(offsetPos);
                if (currentState.getFluidState().isIn(FluidTags.WATER) && instance.state.contains(Properties.WATERLOGGED)) {
                    instance.state = instance.state.with(Properties.WATERLOGGED, true);
                }
                if (!currentState.isAir() && currentState.getMaterial().isReplaceable()) {
                    world.breakBlock(offsetPos, true, user);
                }
                world.setBlockState(offsetPos, instance.state);
            }
            if (!instance.state.isAir()) spawnParticles(world, offsetPos);
            if (instance.isBlockEntity()) {
                instance.entityTag.putInt("x", offsetPos.getX());
                instance.entityTag.putInt("y", offsetPos.getY());
                instance.entityTag.putInt("z", offsetPos.getZ());
                BlockEntity be = world.getBlockEntity(offsetPos);
                be.fromTag(instance.state, instance.entityTag);
            }
        });
        if (result[0] == Result.SUCCESS) {
            ((ServerWorld) world).playSound(null, startPos, SoundEvents.ENTITY_GENERIC_EXPLODE, SoundCategory.BLOCKS, 4.0F, 0f);
        }
        this.entity_container.spawn(world, startPos);
        this.entity_container.clear();
        this.container.clear();
        this.hashcode = container.hashCode();
        return result[0];
    }

    @Override
    public Result releaseBlocks(ServerWorld world, BlockPos startPos, PlayerEntity user) {
        if (ModCompat.shouldTestForCompat()) {
            Result result = Result.SUCCESS;
            if (!(user != null && user.hasPermissionLevel(2) && user.isCreative())) {
                result = ModCompat.testForProtection(startPos, (ServerWorld) world, user, this);
                if (result != Result.SUCCESS) return result;
            }
        }
        return unprotectedReleaseBlocks(world, startPos, user);
    }

    private void spawnParticles(ServerWorld world, BlockPos copy) {
        if (world.random.nextInt(2) == 0) {
            ((ServerWorld) world).spawnParticles((ParticleEffect) ParticleTypes.EXPLOSION, copy.getX(), copy.getY(), copy.getZ(), 1, world.random.nextFloat() - 0.5F, world.random.nextFloat() - 0.5F, world.random.nextFloat() - 0.5F, world.random.nextFloat() - 0.5F);
        } else {
            ((ServerWorld) world).spawnParticles((ParticleEffect) ParticleTypes.POOF, copy.getX(), copy.getY(), copy.getZ(), 5, world.random.nextFloat() - 0.5F, world.random.nextFloat() - 0.5F, world.random.nextFloat() - 0.5F, world.random.nextFloat() - 0.5F);
        }
    }

    @Override
    public void setWidth(int i) {
        width = i;
    }

    @Override
    public void setHeight(int i) {
        height = i;
    }

    @Override
    public void setDepth(int i) {
        depth = i;
    }

    // #region Serialization
    public static final String HASH = "a", LIST = "c", X = "x", Y = "y", Z = "z", COLOR = "d", PLACE_AIR = "e", NAME = "f", MAPSTATE = "g", MAPINT = "h", COMPRESSIONMAP = "i";

    static final int version = 3;

    @Override
    @SuppressWarnings({ "unused", "all" })
    public void writeToNbt(CompoundTag tag) {
        try {
            toTagVersion3(tag);
        } catch (NullPointerException e) {
            e.printStackTrace();
            ModInit.logger.error("Error serializing dynocap v3");
        }
    }

    @Override
    public void readFromNbt(CompoundTag tag) {
        int provided = tag.getByte("version");
        switch (provided) {
        case 0:
            fromTagVersion0(tag);
            break;
        case 1:
            fromTagVersion1(tag);
            break;
        case 2:
            FastByteArrayInputStream stream = new FastByteArrayInputStream(tag.getByteArray("v2"));
            try {
                fromTagVersion1(NbtIo.readCompressed(stream));
            } catch (IOException e) {
                e.printStackTrace();
            }
            break;
        case 3:
            fromTagVersion3(tag);
            break;
        }
    }

    public CompoundTag toTagVersion3(CompoundTag tag) {
        tag.putByte("version", (byte) 3);
        tag.putBoolean(PLACE_AIR, this.shouldPlaceAir);
        tag.putInt(COLOR, this.getColor());
        tag.putShort(X, (short) getWidth());
        tag.putShort(Y, (short) getHeight());
        tag.putShort(Z, (short) getDepth());
        tag.putInt(HASH, hashcode);
        tag.putString(NAME, name);
        FastByteArrayOutputStream stream = new FastByteArrayOutputStream();
        try {
            CompoundTag conta = this.container.toTag(new CompoundTag());
            NbtIo.writeCompressed(conta, stream);
        } catch (IOException | NullPointerException e) {
            throw new RuntimeException(e);
        }
        tag.putByteArray("v3", stream.array);
        return tag;
    }

    public void fromTagVersion3(CompoundTag tag) {
        this.width = tag.getShort(X);
        this.height = tag.getShort(Y);
        this.depth = tag.getShort(Z);
        this.hashcode = tag.getInt(HASH);
        this.color = tag.getInt(COLOR);
        this.shouldPlaceAir = tag.getBoolean(PLACE_AIR);
        this.name = tag.getString(NAME);
        FastByteArrayInputStream stream = new FastByteArrayInputStream(tag.getByteArray("v3"));
        try {
            this.container.fromTag(NbtIo.readCompressed(stream));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    Int2ObjectMap<BlockState> instanceCompressionMap = new Int2ObjectOpenHashMap<>();

    /**
     * Legacy support.
     *
     * @param tag
     */
    private void fromTagVersion1(CompoundTag tag) {
        instanceCompressionMap.clear();
        width = tag.getShort(X);
        height = tag.getShort(Y);
        depth = tag.getShort(Z);
        hashcode = tag.getInt(HASH);
        color = tag.getInt(COLOR);
        shouldPlaceAir = tag.getBoolean(PLACE_AIR);
        this.name = tag.getString(NAME);
        ListTag storageList = (ListTag) tag.get(LIST);
        if (storageList == null)
            storageList = new ListTag();
        ListTag compressionList = (ListTag) tag.get(COMPRESSIONMAP);
        if (compressionList == null)
            compressionList = new ListTag();
        // storage.clear();
        this.cache = null;
        this.renderer = null;

        compressionList.forEach((ct) -> {
            instanceCompressionMap.put(((CompoundTag) ct).getInt(MAPINT), BlockContainer.deserializeBlockState(((CompoundTag) ct).get(MAPSTATE)));
        });
        storageList.forEach((ct) -> {
            CompoundTag c = (CompoundTag) ct;
            BlockPos pos = BlockPos.fromLong(c.getLong(POS));
            CompoundTag etag = c.getCompound(ENTITY_TAG);
            BlockState state = instanceCompressionMap.getOrDefault(c.getInt(STATE), Blocks.CAVE_AIR.getDefaultState());
            if (state == Blocks.CAVE_AIR.getDefaultState()) {
                ModInit.logger.error("Error: converting BlockStorage to BlockContainer");
                state = Blocks.AIR.getDefaultState();
            }
            this.container._rawAdd(state, pos, etag);
        });
        this.container.trim();
    }
    public static final String POS = "a", STATE = "b", TYPE = "c", ENTITY_TAG = "d";

    /**
     * Legacy support.
     *
     * @param tag
     */
    public void fromTagVersion0(CompoundTag tag) {
        width = tag.getShort(X);
        height = tag.getShort(Y);
        depth = tag.getShort(Z);
        hashcode = tag.getInt(HASH);
        color = tag.getInt(COLOR);
        shouldPlaceAir = tag.getBoolean(PLACE_AIR);
        this.name = tag.getString(NAME);
        ListTag lt = (ListTag) tag.get(LIST);
        if (lt == null)
            lt = new ListTag();
        // storage.clear();
        this.cache = null;
        this.renderer = null;
        lt.forEach((rawtag) -> {
            CompoundTag t = (CompoundTag) rawtag;
            BlockPos pos = BlockPos.fromLong(t.getLong(POS));
            CompoundTag etag = t.getCompound(ENTITY_TAG);
            BlockState state = BlockContainer.deserializeBlockState(t.get(STATE));
            this.container._rawAdd(state, pos, etag);
        });
        this.container.trim();
    }
    // #endregion

    @Override
    public boolean isFilled() {
        return !container.isEmpty();
    }

    @Override
    public boolean equals(Object other) {
        return other instanceof IDynocapComponent && ((IDynocapComponent) other).hashCode() == this.hashCode();
    }

    @Override
    public int getColor() {
        return color;
    }

    @Override
    public void setColor(int i) {
        this.color = i;
    }

    @Override
    public boolean getPlaceAir() {
        return shouldPlaceAir;
    }

    @Override
    public void setPlaceAir(boolean bl) {
        this.shouldPlaceAir = bl;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String s) {
        this.name = s;
    }

    @Override
    public boolean shouldPlaceAir() {
        return shouldPlaceAir;
    }

    @Override
    public void setShouldPlaceAir(boolean b) {
        shouldPlaceAir = b;
    }

    @Environment(EnvType.CLIENT)
    DynocapRenderer renderer;
    @Environment(EnvType.CLIENT)
    AgnosticRenderContext cache;

    @Override
    @Environment(EnvType.CLIENT)
    public DynocapRenderer getRenderer() {
        if (this.renderer == null) this.renderer = new DynocapRenderer();
        return renderer;
    }

    @Override
    @Environment(EnvType.CLIENT)
    public AgnosticRenderContext getCache() {
        if (this.cache == null) this.cache = new AgnosticRenderContext();
        return this.cache;
    }
}
