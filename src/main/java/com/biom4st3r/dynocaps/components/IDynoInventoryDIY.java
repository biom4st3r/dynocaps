package com.biom4st3r.dynocaps.components;

import com.biom4st3r.dynocaps.ModInit;

import dev.onyxstudios.cca.api.v3.component.ComponentKey;
import dev.onyxstudios.cca.api.v3.component.ComponentRegistry;
import dev.onyxstudios.cca.api.v3.component.CopyableComponent;
import net.minecraft.inventory.Inventory;
import net.minecraft.util.Identifier;

public interface IDynoInventoryDIY extends Inventory, CopyableComponent<IDynoInventoryDIY> {
    ComponentKey<IDynoInventoryDIY> TYPE = ComponentRegistry.getOrCreate(new Identifier(ModInit.MODID, "inventory_component"), IDynoInventoryDIY.class);

    @Override
    default int size() {
        return 4;
    }
}
